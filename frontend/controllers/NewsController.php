<?php

namespace frontend\controllers;
use Yii;
use yii\web\Controller;
use frontend\models\News;
class NewsController extends Controller{
    function newsData(){
     $query = News::find();
     $newsitems = $query->orderBy('title')->limit('10')->all();
     return $newsitems;
   }
   function actionIndex(){     
     $newsitems_latest = $this->newsData();
     return $this->render('list', ['newsitems_latest' => $newsitems_latest]);
   }
   function actionDetail($id)
   {
     $newsList = $this->newsData();
     $item = null;
     foreach($newsList as $n)
     {
     if($id == $n['id']) $item = $n;
     }
     return $this->render('detail', ['item' => $item]);
     }
}