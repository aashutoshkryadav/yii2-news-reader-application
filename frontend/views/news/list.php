<style>.media-left{margin-right:10px;}</style>
<h1>Latest News</h1>
  <?php foreach ($newsitems_latest as $newsitem_latest){ ?>
  <div class="media">
  <div class="media-left">
  <a href="<?php echo $newsitem_latest['title']; ?>">
  <img class="media-object"  src="<?php echo $newsitem_latest['feature_picture'];?>"  
  style="width: 120px;">
  </a>
  </div>
  <div class="media-body">
  <h4 class="media-heading">
  <em><?php echo Yii::$app->formatter->asDatetime($newsitem_latest['date'], "php:d/m/Y"); ?>
  </em> | <a href="<?php echo Yii::$app->urlManager->createUrl(['news/detail' , 'id'
  => $newsitem_latest['id']]) ?>"><strong><?php echo $newsitem_latest['title']; ?></strong></a></h4>
  <p><?php echo $newsitem_latest['short_description']; ?>
  <br/>
  <a href="<?php echo Yii::$app->urlManager->createUrl(['news/detail' , 'id'
  => $newsitem_latest['id']]) ?>">Read More...</a></p>
  </div>
  </div>
  <?php } ?>
