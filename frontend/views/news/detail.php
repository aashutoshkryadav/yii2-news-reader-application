<div class="panel panel-default">
<div class="panel-heading">
<?php echo Yii::$app->formatter->asDatetime($item['date'], "php:d/m/Y"); ?></div>
<div class="panel-body">
<p><img src="<?php echo $item['feature_picture'] ?>" width="90%"/></p>
<p><strong><?php echo $item['title'] ?></strong></p>
<p><?php echo $item['description'] ?></p>
</div>
</div>